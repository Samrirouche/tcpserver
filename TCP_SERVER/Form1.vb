Public Class Form1 : Implements onReceiveDataListener
    Public ServerThread As Threading.Thread = Nothing


    ' i'm gonna create access point 

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' start listener
        Dim ip As String = ComboBox1.Text
        Dim port As Integer = Integer.Parse(TextBox1.Text)
        TCP.setIPAddress(ip)
        TCP.setPort(port)
        If ServerThread Is Nothing Then
            ServerThread = New Threading.Thread(AddressOf TCP.CreateServer)
            ServerThread.Start()
            ListBox1.Items.Add("Server started now !")
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ' stop server
        If TCP.StopServer Then
            If Not ServerThread Is Nothing Then
                ServerThread.Abort()
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If TextBox2.Text.Length > 0 Then
            TCP.SendData(TextBox2.Text)
            ListBox1.Items.Add(TextBox2.Text)
            TextBox2.Clear()
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' we load ip addresses
        For Each ip As String In TCP.getDefaultIPs
            ComboBox1.Items.Add(ip)
        Next
    End Sub

    'override
    Public Sub onDataReceived(ByVal data As String) Implements onReceiveDataListener.onDataReceived
        Me.Invoke(New Action(Of String)(AddressOf DisplayData), data)
    End Sub
    Public Sub onClientConnected() Implements onReceiveDataListener.onClientConnected
        Me.Invoke(New Action(Of String)(AddressOf DisplayData), "New Client connected !")
    End Sub

    Public Sub DisplayData(ByVal data As String)
        ListBox1.Items.Add(data)
    End Sub
End Class
