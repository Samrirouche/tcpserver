Imports System.IO
Imports System.Net
Module TCP
    Interface onReceiveDataListener
        Sub onDataReceived(ByVal data As String)
        Sub onClientConnected()
    End Interface

    Private Server As Sockets.TcpListener = Nothing
    Private client As Sockets.TcpClient = Nothing
    Private listener As onReceiveDataListener = Form1

    Private IP_ As String = Nothing
    Private Port_ As Integer = Nothing

    ' we add methods set/get
    Public Sub setPort(ByVal p As Integer)
        Port_ = p
    End Sub

    Public Sub setIPAddress(ByVal ip As String)
        IP_ = ip
    End Sub
    Public Sub CreateServer()
        Try
            StopServer()
            Dim IP As IPAddress = IPAddress.Parse(IP_)
            Server = New Sockets.TcpListener(IP, Port_)
            ' Start listening for client requests.
            Server.Start()

            Dim bytes(1024) As [Byte]
            Dim data As [String] = Nothing

            ' Enter the listening loop.
            While True
                client = Server.AcceptTcpClient()
                listener.onClientConnected()

                data = Nothing

                ' Get a stream object for reading and writing
                Dim stream As Sockets.NetworkStream = client.GetStream()
                Dim i As Int32

                ' Loop to receive all the data sent by the client.
                i = stream.Read(bytes, 0, bytes.Length)
                While (i <> 0)
                    ' Translate data bytes to a ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i)
                    ' pass data to remote control
                    listener.onDataReceived(data.ToString)
                    i = stream.Read(bytes, 0, bytes.Length)
                End While
                ' Shutdown and end connection
                client.Close()
            End While
        Catch e As Sockets.SocketException

        Catch io As IOException

        End Try
    End Sub

    Public Function getDefaultIPs() As List(Of String)
        Dim host As String = System.Net.Dns.GetHostName()
        Dim count As Integer = System.Net.Dns.GetHostEntry(host).AddressList().Length()
        Dim list As List(Of String) = New List(Of String)
        For i As Integer = 1 To count - 1
            Dim LocalHostaddress As String = System.Net.Dns.GetHostEntry(host).AddressList(i).ToString()
            If LocalHostaddress.Contains("192.168") Then
                list.Add(LocalHostaddress)
            End If
        Next

        Return list
    End Function
    Public Function StopServer() As Boolean
        If Not server Is Nothing Then
            server.Stop()
            server = Nothing
            If Not client Is Nothing Then
                client.Close()
                client = Nothing
            End If
            Return True
        End If
        Return False
    End Function
    Public Sub SendData(ByVal message As String)
        If client.Connected = True Then
            Dim stream As Sockets.NetworkStream = client.GetStream()
            Dim writer As BinaryWriter = New BinaryWriter(client.GetStream())
            writer.Write(message & vbNewLine)
        End If
    End Sub

    Public Function isClientConnected() As Boolean
        If Not client Is Nothing Then
            Return client.Connected
        End If
        Return False
    End Function
End Module
